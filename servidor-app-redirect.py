#!/usr/bin/python3

import socket
import random

numeroURL = random.randint(1, 3)
direcciones = {1: "http://gsyc.es",
               2: "http://www.urjc.es", 3: "https://www.google.es/"}
print("La dirección de la redirección es: " + str(direcciones[numeroURL]))

mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
mySocket.bind((socket.gethostbyname('localhost'), 1234))

mySocket.listen(15)

try:
    while True:
        print('Waiting for connections')
        (recvSocket, address) = mySocket.accept()
        print('Request received:')
        print(recvSocket.recv(2048))
        print('Answering back...')

        recvSocket.send(b"HTTP/1.1 302 Found\r\nLocation: " +
                        bytes(str(direcciones[numeroURL]), 'utf-8') +
                        b"\r\n\r\n")
        recvSocket.close()

except KeyboardInterrupt:
    print("Closing binded socket")
    mySocket.close()
